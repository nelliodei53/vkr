import sqlite3

# Устанавливаем соединение с базой данных
connection = sqlite3.connect('QA_users.db')
cursor = connection.cursor()

# Создаем таблицу Users
cursor.execute('''
CREATE TABLE IF NOT EXISTS QA (
id INTEGER PRIMARY KEY,
Question TEXT NOT NULL,
Answer TEXT,
Method TEXT               
)
''')

# Сохраняем изменения и закрываем соединение
connection.commit()
connection.close()
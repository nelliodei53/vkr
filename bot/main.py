import sqlite3
import telebot
from nbconvert.preprocessors import ExecutePreprocessor
import nbformat
import time
from telebot import types
from telebot import TeleBot, types
from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup

bot = telebot.TeleBot('6358530150:AAE919TQOeBLw39Jw6N23qcqDDvH2vvuGZk')

# @bot.message_handler(commands=['start'])
# def start_message(message):
# 	bot.send_message(message.chat.id, 'Добро пожаловать, выберите способ получения ответа в меню')

conn = sqlite3.connect('QA_users.db', check_same_thread=False)
cursor = conn.cursor()

def db_table_val(Question: str):
	values = (Question,)
	cursor.execute('SELECT * FROM QA')
	cursor.execute('INSERT INTO QA (Question) VALUES (?)', values)
	conn.commit()

def run_ipynb(ipynb_file):
    with open(ipynb_file, 'r', encoding='utf-8') as f:
        nb = nbformat.read(f, as_version=4)
    ep = ExecutePreprocessor(timeout=600, kernel_name='python3')  
    ep.preprocess(nb, {'metadata': {'path': '.'}})

user_states = {}

@bot.message_handler(commands=["start"])
def start(message):
    key = types.InlineKeyboardMarkup()
    but_1 = types.InlineKeyboardButton(text="bert", callback_data="bert")
    but_2 = types.InlineKeyboardButton(text="squad_ru_bert", callback_data="squad_ru_bert")
    key.add(but_1, but_2)
    bot.send_message(message.chat.id, "Выберите кнопку:", reply_markup=key)
    user_states[message.chat.id] = None

@bot.callback_query_handler(func=lambda c: True)
def callback_handler(c):
    user_id = c.message.chat.id
    if c.data == 'bert':
        bot.send_message(user_id, 'Задайте вопрос')
        user_states[user_id] = 'waiting_for_answer_in_bert'

    if c.data == 'sqad_ru_bert':
        bot.send_message(user_id, 'Задайте вопрос')
        user_states[user_id] = 'waiting_for_answer'

@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_for_answer_in_bert')
def handle_answer(message):
    bot.send_message(message.chat.id, 'Вопрос обрабатывается...')
    user_question = message.text
    db_table_val(Question=user_question)
    run_ipynb('question_processing.ipynb') 
    cursor.execute("SELECT Answer FROM QA WHERE id = (SELECT MAX(id) FROM QA)")
    result = cursor.fetchone()
    bot.send_message(message.chat.id, result)
    user_states[message.chat.id] = None

@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_for_answer')
def handle_answer(message):
    bot.send_message(message.chat.id, 'Вопрос обрабатывается...')
    user_question = message.text
    db_table_val(Question=user_question)
    run_ipynb('squad_ru.ipynb') 
    cursor.execute("SELECT Answer FROM QA WHERE id = (SELECT MAX(id) FROM QA)")
    result = cursor.fetchone()
    bot.send_message(message.chat.id, result)
    user_states[message.chat.id] = None


bot.polling()
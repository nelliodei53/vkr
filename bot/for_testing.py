from telebot import TeleBot, types
from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup

bot = TeleBot("6358530150:AAE919TQOeBLw39Jw6N23qcqDDvH2vvuGZk")

user_states = {}  # Словарь для отслеживания состояний пользователей

@bot.message_handler(commands=["start"])
def start(message):
    key = types.InlineKeyboardMarkup()
    but_1 = types.InlineKeyboardButton(text="NumberOne", callback_data="NumberOne")
    but_2 = types.InlineKeyboardButton(text="NumberTwo", callback_data="NumberTwo")
    key.add(but_1, but_2)
    bot.send_message(message.chat.id, "Выберите кнопку:", reply_markup=key)
    user_states[message.chat.id] = None

@bot.callback_query_handler(func=lambda c: True)
def callback_handler(c):
    user_id = c.message.chat.id
    if c.data == 'NumberOne':
        bot.send_message(user_id, 'Вы выбрали кнопку 1')
        user_states[user_id] = 'waiting_for_answer'

    if c.data == 'NumberTwo':
        bot.send_message(user_id, 'Вы выбрали кнопку 2')

@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_for_answer')
def handle_answer(message):
    if message.text == "Ответ на сообщение 'Это кнопка 1'":
        bot.send_message(message.chat.id, "Вы ответили на сообщение 'Это кнопка 1'")
    else:
        bot.send_message(message.chat.id, "Пожалуйста, отправьте правильный ответ.")

    user_states[message.chat.id] = None

if __name__ == "__main__":
    bot.polling(none_stop=True)